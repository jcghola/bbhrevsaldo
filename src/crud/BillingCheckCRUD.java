package crud;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pojos.BillingCheckClientesSaldoDiaRevNoc;
import pojos.BillingCheckNumConCostoV;
import pojos.BillingCheckTransaccionesConsultaSaldo;
import pojos.BillingVelocidad;
import pojos.ClientesBillingConsultarSaldo;
import pojos.ClientesMtGt;
import until.HibernateUtil;

/**
 *
 * @author Jsus
 */

public class BillingCheckCRUD {
    Session session = null;
    Transaction tx=null;
    
    public BillingCheckCRUD(){
        try{
            this.session = HibernateUtil.getSessionFactory().openSession();  
        }catch (HibernateException e) {
            throw new HibernateException(e);            
        }
    }    
      
    public List<ClientesBillingConsultarSaldo> getClientesMtGt(String Fecha, String Hora) throws HibernateException{
        List<ClientesBillingConsultarSaldo> ListaClientes = null;
        try {        
            iniciaOperacion();
            String HQL="from ClientesBillingConsultarSaldo CS where not exists (from BillingCheckTransaccionesConsultaSaldo T where T.msgIndexOrigen = CS.id and T.fecha >='"+Fecha+" "+Hora+"')";
            Query q = session.createQuery (HQL);
            ListaClientes = (List<ClientesBillingConsultarSaldo>) q.list(); 
            tx.commit();
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
        return ListaClientes;
    }    
   
    public void setBillingCheckClientesSaldoDiaRevNoc(String num_movil, Integer id_cliente, Long id_transaction)throws HibernateException{
        //num_movil,id_servicio
        try {
            iniciaOperacion();
            BillingCheckClientesSaldoDiaRevNoc BC= new BillingCheckClientesSaldoDiaRevNoc();
            BC.setNumMovil(num_movil);
            BC.setIdCliente(id_cliente);
            BC.setNumCorto("99661");
            BC.setIdTransaction(id_transaction);
            BC.setFechaConSaldo(new Date());
            BC.setFechaRevision(new Date());
            session.save(BC);
            session.flush();
            session.clear();
            tx.commit();
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
    }    
    
    public ClientesMtGt getClientesMtGtId(int idCliente)throws HibernateException{
        ClientesMtGt he = null;
        try {
            iniciaOperacion();            
            he=(ClientesMtGt)session.get(ClientesMtGt.class, idCliente);
            session.flush();
            session.clear();
            tx.commit();     
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
        return he;
    }
    
        public void delClientesMtGt(ClientesBillingConsultarSaldo cliente)throws HibernateException{
        try {
            iniciaOperacion();            
            session.delete(cliente);
            session.flush();
            session.clear();
            tx.commit();     
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }

    }
    
    public int eliminaUnoClientesControl(Integer ID)throws HibernateException{        
        int deletedEntities=0; 
        try {
            iniciaOperacion();
            String hqlDelete = "DELETE BillingCheckClientesSaldoDiaRevNocIds WHERE idCliente = "+ID;
            deletedEntities = session.createQuery( hqlDelete ).executeUpdate(); 
            session.flush();
            session.clear();
            tx.commit();    
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
        return deletedEntities;
    }
    
    public int updateBillingCheckClientesSaldoDiaRevNocTranNull()throws HibernateException{        
        int deletedEntities=0; 
        try {
            iniciaOperacion();
            String hqlDelete = "UPDATE BillingCheckClientesSaldoDiaRevNoc SET idTransaction=NULL";
            deletedEntities = session.createQuery( hqlDelete ).executeUpdate(); 
            session.flush();
            session.clear();
            tx.commit();    
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
        return deletedEntities;
    }
    
    public void insertaClientesControl()throws SQLException{        
        try {
            iniciaOperacion();
            String hqlUpdate =  "INSERT INTO BillingCheckClientesSaldoDiaRevNocIds (Id_Cliente)"
                    + " SELECT Id_Cliente from Clientes_MT_GT where Id_Clave in (20133,20185,20136) and id_Status=20 and carrier like '/SMPP/COMCEL_GT%'";
            CallableStatement cstmt=session.connection().prepareCall(hqlUpdate);
            cstmt.executeUpdate();
            tx.commit();            
        }catch (SQLException e) {
            if (tx != null) {tx.rollback();}
            throw new SQLException(e);         
        }
    }

    public int eliminaTodosClientesControl()throws HibernateException{        
        int updatedEntities=0;
        try {
            iniciaOperacion();
            String hqlDelete =  "delete BillingCheckClientesSaldoDiaRevNocIds";
            updatedEntities = session.createQuery( hqlDelete ).executeUpdate(); 
            session.flush();
            session.clear();
            tx.commit();            
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
        return updatedEntities;
    }
    
    public BillingCheckClientesSaldoDiaRevNoc getClienteConSaldo(Integer idCliente)throws HibernateException{
        BillingCheckClientesSaldoDiaRevNoc he = null;
        try {
            iniciaOperacion();
            String hqlSelect = "from BillingCheckClientesSaldoDiaRevNoc b where b.idCliente = "+idCliente;
            List r = session.createQuery( hqlSelect ).setMaxResults(1).list();
            if(r.size()>0){he=(BillingCheckClientesSaldoDiaRevNoc)r.get(0);}
            session.flush();
            session.clear();
            tx.commit();     
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
        return he;
    }
    
    public void updateBillingCheckClientesSaldoDiaRevNoc(BillingCheckClientesSaldoDiaRevNoc bh)throws HibernateException{

        try {
            iniciaOperacion();
            session.update(bh);
            session.flush();
            session.clear();
            tx.commit();     
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
    }
    
    public void updateClientesMtGt(ClientesMtGt bh)throws HibernateException{

        try {
            iniciaOperacion();
            session.update(bh);
            session.flush();
            session.clear();
            tx.commit();     
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
    }
    
    public Map<String,BillingCheckNumConCostoV> getNumConCosto(int PorcentajeRecobro)throws HibernateException{        

        Map<String,BillingCheckNumConCostoV> m=new HashMap<>();
        try {
            iniciaOperacion();
            String hqlSelect = "from BillingCheckNumConCostoV where porcentaje="+PorcentajeRecobro;
            List l = session.createQuery( hqlSelect ).list();            
            tx.commit(); 
            for (BillingCheckNumConCostoV row: (List<BillingCheckNumConCostoV>) l ) {
               m.put(row.getCarrier().trim()+row.getNumCortoO().trim(), new BillingCheckNumConCostoV(row) );
               //System.out.println("\n"+row.getId()+" "+row.getCarrier()+" "+row.getNumCortoO()+" "+row.getNumCortoD()+" "+row.getCosto());
             
            }
            //System.out.println(m.size() + " "+ l.size());
            l.clear();
            
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
        return m;
    }
    
    public void setBillingCheck(Long Id_Transaccion,String Comment,String Envio,String DATAXML,Integer result,Integer ID,Integer MsgIndexDestino,Integer Id_Envio)throws HibernateException{
        try {
            iniciaOperacion();
            BillingCheckTransaccionesConsultaSaldo BResponse= new BillingCheckTransaccionesConsultaSaldo();
            BResponse.setIdTransaccion(Id_Transaccion);
            BResponse.setComment(Comment);
            BResponse.setEnvio(Envio);
            BResponse.setDataxml(DATAXML);
            BResponse.setResult(result);
            BResponse.setMsgIndexOrigen(ID);
            BResponse.setMsgIndexDestino(MsgIndexDestino);
            BResponse.setFecha(new Date());           
            BResponse.setIdEnvio(Id_Envio);
            session.save(BResponse);
            session.flush();
            session.clear();
            tx.commit();
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
    }
    
    public List getBillingcheckResultado(String fechaHoy,String horaInicio)throws HibernateException{        
        List resultado=null;
        try {
            iniciaOperacion();
            String hqlSelect = "select b.comment,COUNT(b) from BillingCheckTransaccionesConsultaSaldo b where b.fecha > '"+fechaHoy+" "+horaInicio+"' group by b.comment";
            resultado = session.createQuery( hqlSelect ).list();
            session.flush();
            session.clear();
            tx.commit();     
        }catch (HibernateException e) {
            manejaExcepcion(e);
            throw e;             
        }
        return resultado;
    }
    
    public List getTPS(String FechaInicio, String HoraInicio,Date fin)throws SQLException{        
        List l=null;
        DateFormat f = new SimpleDateFormat("yyyyMMdd HH:mm:ss");        
        try {
            String consulta = "SELECT DATEPART(HH,fecha)H,DATEPART(MI,fecha)M, CAST(ROUND((CAST(COUNT(*) as float) /60),3)AS VARCHAR(7)) TT " +
                    "FROM BillingCheckTransacciones_ConsultaSaldo " +
                    "WHERE fecha BETWEEN '"+FechaInicio+" "+HoraInicio+"' AND '"+f.format(fin)+"'" +
                    "GROUP BY DATEPART(HH,Fecha),DATEPART(MI,Fecha) " +
                    "ORDER BY 1 DESC, 2 DESC";
            CallableStatement cstmt=session.connection().prepareCall(consulta);
            ResultSet r = cstmt.executeQuery();
            l = new ArrayList();
            while(r.next()){                
                l.add(new BillingVelocidad(r.getInt("H"),r.getInt("M"),r.getString("TT")));
            }
        }catch (SQLException e) {            
            throw e;             
        }
        return l;
    }

    private void manejaExcepcion(HibernateException he) throws HibernateException
    {
        tx.rollback();
        throw new HibernateException("Ocurrió un error en la capa de acceso a datos", he);
    }
    
    private void iniciaOperacion() throws HibernateException
    {
        //session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }
    
    public void closeSession() throws HibernateException
    {
        session.close();
    }
}
