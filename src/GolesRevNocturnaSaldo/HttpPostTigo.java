package GolesRevNocturnaSaldo;

import java.io.IOException;
import java.io.InterruptedIOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicInteger;
import javax.net.ssl.SSLException;
import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.HttpException;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

/**
 *
 * @author Jsus
 */

public class HttpPostTigo {
    
    private CloseableHttpClient httpclient;
    private URI uri;
    private HttpPost httppost;
    private String XML;
    private RequestConfig requestConfig;
    private HttpClientContext localContext;
    private AtomicInteger count;

    public HttpPostTigo(PoolingHttpClientConnectionManager cm) {        
        try{            
            httpclient = HttpClients
                    .custom()
                    .disableConnectionState()
                    .setKeepAliveStrategy(keepAliveStrat) 
                    .setRetryHandler(myRetryHandler)
                    .addInterceptorLast(new HttpRequestInterceptor() {
                        @Override
                        public void process(final HttpRequest request,final HttpContext context) throws HttpException, IOException {
                            AtomicInteger count = (AtomicInteger) context.getAttribute("count");
                            request.addHeader("Count", Integer.toString(count.getAndIncrement()));
                        }
                    })
                    .setConnectionManager(cm)
                    .build();
            
            count = new AtomicInteger(1);
            localContext = HttpClientContext.create();
            localContext.setAttribute("count", count);
            
            requestConfig = RequestConfig.custom()
                    .setSocketTimeout(5*60*1000)
                    .setConnectTimeout(5*60*1000)
                    .setConnectionRequestTimeout(5*60*1000)
                    .build();
            
            uri = new URIBuilder()   
                    .setScheme("http")
                    .setHost("providerservices.tigo.com.gt")
                    .setPath("/billingcheck")
                    .setPort(8080)
                    .build();

            httppost = new HttpPost(uri);
            httppost.setProtocolVersion(HttpVersion.HTTP_1_0);
            httppost.setHeader("Content-Type","text/xml");
            httppost.setHeader("Connection","keep-alive");
            httppost.setHeader("Host","providerservices.tigo.com.gt");
            httppost.setConfig(requestConfig);
            
        }catch(URISyntaxException e){
        }
        
    }
    ConnectionKeepAliveStrategy keepAliveStrat = new ConnectionKeepAliveStrategy() {

    @Override
    public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
        // Honor 'keep-alive' header
        HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
        while (it.hasNext()) {
            HeaderElement he = it.nextElement();
            String param = he.getName();
            String value = he.getValue();
            if (value != null && param.equalsIgnoreCase("timeout")) {
                try {
                    //System.out.println("Alive:"+Long.parseLong(value) * 1000);
                    return Long.parseLong(value) * 1000;
                } catch(NumberFormatException ignore) {
                }
            }
        }
        HttpHost target = (HttpHost) context.getAttribute(HttpClientContext.HTTP_TARGET_HOST);
        if ("providerservices.tigo.com.gt".equalsIgnoreCase(target.getHostName())) {
            // Keep alive for 5 seconds only
            //System.out.println("Alive:"+5000);
            return 5 * 1000;
        } else {
            // otherwise keep alive for 30 seconds
            //System.out.println("Alive:"+30000);
            return 30 * 1000;
        }
    }

};
    
    public String Go()throws UnsupportedEncodingException,IOException{
        try{
            StringEntity postParamsEntity = new StringEntity(XML); 
            postParamsEntity.setContentType("text/xml");            
            httppost.setEntity(postParamsEntity);            
            CloseableHttpResponse response = httpclient.execute(httppost,localContext);            
            HttpEntity entity = response.getEntity();                
            return response+EntityUtils.toString(entity); 
        }catch(Exception e){e.printStackTrace();      
            StringWriter errors = new StringWriter();
            e.printStackTrace(new PrintWriter(errors));        
            return errors.toString().replaceAll("\"", "").replaceAll("'", "");
        }       
    }
    
    public void Close(){
        try{
            httppost.releaseConnection();
        }catch(Exception e){
            e.printStackTrace();
        }
    }  

    public void setXML(String XML) {
        this.XML = XML;
    }
    
    HttpRequestRetryHandler myRetryHandler = new HttpRequestRetryHandler() {

    @Override
    public boolean retryRequest(
            IOException exception,
            int executionCount,
            HttpContext context) {
        if (executionCount >= 5) {
            // Do not retry if over max retry count
            return false;
        }
        if (exception instanceof InterruptedIOException) {
            // Timeout
            return false;
        }
        if (exception instanceof UnknownHostException) {
            // Unknown host
            return false;
        }
        if (exception instanceof ConnectTimeoutException) {
            // Connection refused
            return false;
        }
        if (exception instanceof SSLException) {
            // SSL handshake exception
            return false;
        }
        HttpClientContext clientContext = HttpClientContext.adapt(context);
        HttpRequest request = clientContext.getRequest();
        boolean idempotent = !(request instanceof HttpEntityEnclosingRequest);
        if (idempotent) {
            // Retry if the request is considered idempotent
            return true;
        }
        return false;
    }

};
}
