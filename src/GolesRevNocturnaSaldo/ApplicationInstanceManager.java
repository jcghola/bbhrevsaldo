/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GolesRevNocturnaSaldo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.logging.*;


/**
 *
 * @author Jsus
 */

public class ApplicationInstanceManager {

    private static ApplicationInstanceListener subListener;

    /** Randomly chosen, but static, high socket number */
    public static final int SINGLE_INSTANCE_NETWORK_SOCKET = 44331;

    /** Must end with newline */
    public static final String SINGLE_INSTANCE_SHARED_KEY = "BBH.BBH";
    
    
        
    

    /**
     * Registers this instance of the application.
     * 
     * @return true if first instance, false if not.
     */
    public static boolean registerInstance() {
        // returnValueOnError should be true if lenient (allows app to run on network error) or false if strict.
        boolean returnValueOnError = true;
        // try to open network socket
        // if success, listen to socket for new instance message, return true
        // if unable to open, connect to existing and send new instance message, return false
        Logger logger = Logger.getLogger("com.wombat.nose");
        try {
            
            FileHandler fh = new FileHandler("mylog.txt");
            logger.addHandler(fh);
            logger.setLevel(Level.ALL);
            logger.info("doing stuff");
            
            
            
            InetAddress inteAddress = InetAddress.getByAddress(new byte[] {127, 0, 0, 1});
            final ServerSocket socket = new ServerSocket(SINGLE_INSTANCE_NETWORK_SOCKET, 10, inteAddress);
            //log.debug("Listening for application instances on socket " + SINGLE_INSTANCE_NETWORK_SOCKET);
            
            Thread instanceListenerThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    boolean socketClosed = false;
                    while (!socketClosed) {
                        if (socket.isClosed()) {
                            socketClosed = true;
                        } else {
                            try {
                                Socket client = socket.accept();
                                BufferedReader in = new BufferedReader(new InputStreamReader(client.getInputStream()));
                                String message = in.readLine();
                                if (SINGLE_INSTANCE_SHARED_KEY.trim().equals(message.trim())) {
                                    //log.debug("Shared key matched - new application instance found");
                                    fireNewInstance();
                                }
                                in.close();
                                client.close();
                            } catch (IOException e) {
                                socketClosed = true;
                            }
                        }
                    }
                }
            });
            instanceListenerThread.start();
            // listen
        } catch (UnknownHostException e) {
            logger.log(Level.WARNING, e.getMessage(), e);
            return returnValueOnError;
        } catch (IOException e) {
            //log.debug("Port is already taken.  Notifying first instance.");
            try {
                InetAddress inteAddress = InetAddress.getByAddress(new byte[] {127, 0, 0, 1});
                Socket clientSocket = new Socket(inteAddress, SINGLE_INSTANCE_NETWORK_SOCKET);
                OutputStream out = clientSocket.getOutputStream();
                out.write(SINGLE_INSTANCE_SHARED_KEY.getBytes());
                out.close();
                clientSocket.close();
                //log.debug("Successfully notified first instance.");
                return false;
            } catch (UnknownHostException e1) {
                //log.error(e.getMessage(), e);
                return returnValueOnError;
            } catch (IOException e1) {
                //log.error("Error connecting to local port for single instance notification");
                //log.error(e1.getMessage(), e1);
                return returnValueOnError;
            }

        }
        return true;
    }

    public static void setApplicationInstanceListener(ApplicationInstanceListener listener) {
        subListener = listener;
    }

    private static void fireNewInstance() {
      if (subListener != null) {
        subListener.newInstanceCreated();
      }
  }
    public static void main(String[] args) {
       if (!ApplicationInstanceManager.registerInstance()) {
                    // instance already running.
                    System.out.println("Another instance of this application is already running.  Exiting.");
                    System.exit(0);
       }
       ApplicationInstanceManager.setApplicationInstanceListener(new ApplicationInstanceListener() {
          @Override
          public void newInstanceCreated() {
             System.out.println("New instance detected...");
             // this is where your handler code goes...
          }
       });
    }
}



