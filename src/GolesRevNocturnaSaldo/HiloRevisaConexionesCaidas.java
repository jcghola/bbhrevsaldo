/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GolesRevNocturnaSaldo;

import java.util.concurrent.TimeUnit;
import org.apache.http.conn.HttpClientConnectionManager;

/**
 *
 * @author Jsus
 */
public class HiloRevisaConexionesCaidas extends Thread{
    private final HttpClientConnectionManager connMgr;
        private volatile boolean shutdown;

        public HiloRevisaConexionesCaidas(HttpClientConnectionManager connMgr) {
            super();
            this.connMgr = connMgr;
        }

        @Override
        public void run() {
            try {
                while (!shutdown) {
                    synchronized (this) {
                        wait(3500);
                        connMgr.closeExpiredConnections();
                        connMgr.closeIdleConnections(3, TimeUnit.SECONDS);                    
                    }
                }
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }

        public void shutdown() {
            shutdown = true;
            synchronized (this) {
                notifyAll();
            }
        }
    
}
