/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GolesRevNocturnaSaldo;

import crud.BillingCheckCRUD;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.hibernate.HibernateException;
import org.xml.sax.SAXException;
import pojos.BillingCheckNumConCostoV;
import pojos.ClientesBillingConsultarSaldo;
import pojos.ClientesMtGt;

/*
 *
 * @author Jsus
 *
 */

public class Hilo extends Thread{
    private BillingCheckCRUD Dao;
    private List<ClientesBillingConsultarSaldo> ListaMensajes;
    private PoolingHttpClientConnectionManager cm;
    private Map<String,BillingCheckNumConCostoV> montos;
    private int porcentajeRevision;

    
    public Hilo(List<ClientesBillingConsultarSaldo> ListaMensajes,String hiloName,PoolingHttpClientConnectionManager cm, Map<String,BillingCheckNumConCostoV> montos, int porcentajeRevision){        
        super(hiloName);
        this.ListaMensajes = ListaMensajes;
        this.Dao = new BillingCheckCRUD();
        this.cm=cm;
        this.montos=montos;
        this.porcentajeRevision = porcentajeRevision;
    }
    
    @Override
    public void run() {
        
        String NombreHilo = getName();

        String respuestaTigo;         
        String carrier, num_movil;                
	Integer id_cliente;
        int categoria = 9;
        BillingCheckNumConCostoV monto;
        
        HttpPostTigo post = new HttpPostTigo(cm);
        
        switch(porcentajeRevision){
            case 75: categoria = 11; break; case 50: categoria = 12; break; default: break;
        }
   
        int MensajesAprocesar = ListaMensajes.size();
        salidaCmd("Inicia hilo:"+NombreHilo+", mensajes a procesar:"+MensajesAprocesar);
        
        for( int x = 0 ; x < MensajesAprocesar ; x++ ) { // start from index 0
            ClientesBillingConsultarSaldo men =ListaMensajes.get(x);
            carrier = men.getCarrier();
            num_movil = men.getNumMovil();
            id_cliente=men.getIdCliente();
            monto=montos.get(carrier+men.getNumCorto());
            
            if(monto==null){
                salidaCmd("\nNum corto sin costo definido!!!");              
            }else{
          
                String a= "\nCarrier:"+carrier+"\nId_Cliente:"+id_cliente+"\nNum_movil:"+num_movil+"\nHilo:"+NombreHilo+"\n\n";
                //salidaCmd(a);  
                String XML=	"<?xml version=\"1.0\" encoding=\"UTF-8\"?>"+
                                                "<billingcheck>"+
                                                "<msisdn>"+num_movil+"</msisdn>"+
                                                "<amount>"+monto.getCosto()+"</amount>"+
                                                "</billingcheck>";

                try {                    

                    post.setXML(XML);
                    respuestaTigo = post.Go();

                    //salidaCmd(respuestaTigo+"\n");
                    String[] respuestaArr = respuestaTigo.split("<billingcheck>");		
                    //salidaCmd("\n\nTIENE BILLING:"+respuestaArr.length);

                    if ((!respuestaTigo.trim().equals("")) && respuestaArr.length==2){

                        //salidaCmd("\n\n"+respuesta);
                        XmlToData dataTigo= new XmlToData("<billingcheck>"+respuestaArr[1]);
                        dataTigo.getDataFromXml();

                        String datosDevueltos = "\nHilo:"+NombreHilo+" Mensaje: "+(x+1)+" De:"+MensajesAprocesar
                                //+"\nresponse:"+respuestaTigo
                                +"\ntransid:"+dataTigo.getTransid()
                                +"\nmsisdn:"+dataTigo.getMsisdn()
                                +"\nresult:"+dataTigo.getResult()
                                +"\ncomment:"+dataTigo.getComment();
//historias horribles
                        //salidaCmd(a+datosDevueltos);
                        ClientesMtGt bc = Dao.getClientesMtGtId(id_cliente);

                        if( dataTigo.getResult()==0 || dataTigo.getResult()==1 ){
                            if(bc != null){
                                bc.setIdCategoria(categoria);
                                Dao.updateClientesMtGt(bc);
                                Dao.delClientesMtGt(men);
                                Dao.setBillingCheck(new Long(dataTigo.getTransid()), dataTigo.getComment(), "REVISION NOCTURNA", respuestaTigo, dataTigo.getResult(), men.getId(), null,men.getIdCliente());
                            }                 

                        }else if( dataTigo.getResult()==8 ){      
                            if( dataTigo.getResult()==8 ){ sleep(500); }                              
                        }else{
                            Dao.setBillingCheck(new Long(dataTigo.getTransid()), dataTigo.getComment(), "REVISION NOCTURNA", respuestaTigo, dataTigo.getResult(), men.getId(), null,men.getIdCliente());
                        }                  
                        salidaCmd(datosDevueltos);
                    }else{
                        salidaCmd("\nError de respuesta de TIGO");
                    }

                }catch (IOException | InterruptedException | ParserConfigurationException | HibernateException | SAXException e) {
                        e.printStackTrace();
                }
            }                   
        }
        ListaMensajes.clear();
        Dao.closeSession();
        post.Close();
        salidaCmd("El hilo "+NombreHilo+" acabo, "+MensajesAprocesar+" clientes procesados");
    }
    
    private static void salidaCmd ( String cadena ) {        
        System.out.println(cadena);
    }

}