/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package GolesRevNocturnaSaldo;

import java.io.IOException;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Jsus
 */
public class XmlToData {
    private String xml,msisdn,transid,comment;
    private int result;
    public XmlToData(String xml) {
        this.xml = xml;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public String getTransid() {
        return transid;
    }

    public int getResult() {
        return result;
    }

    public String getComment() {
        return comment;
    }
    
    public void getDataFromXml() throws ParserConfigurationException,SAXException,IOException{
        
        try{            
            DocumentBuilderFactory dbf =DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource();
            is.setCharacterStream(new StringReader(xml));

            Document doc = db.parse(is);
            NodeList nodes = doc.getElementsByTagName("billingcheck");

            Element element = (Element) nodes.item(0);

            NodeList msisdnx = element.getElementsByTagName("msisdn");
            Element line = (Element) msisdnx.item(0);
            msisdn = getCharacterDataFromElement(line);
            //System.out.println("\nnmsisdn:"+msisdn);		
            NodeList transidx = element.getElementsByTagName("transid");
            line = (Element) transidx.item(0);
            transid = getCharacterDataFromElement(line);
            //System.out.println("\ntransid:"+transid);
            NodeList resultx = element.getElementsByTagName("result");
            line = (Element) resultx.item(0);
            result =  Integer.valueOf(getCharacterDataFromElement(line));		
            //System.out.println("\nresult:"+result);
            NodeList commentx = element.getElementsByTagName("comment");
            line = (Element) commentx.item(0);
            comment = getCharacterDataFromElement(line);
        }catch(  ParserConfigurationException | SAXException | IOException e){
            throw e;
        }
        
    
    }
    
    private String getCharacterDataFromElement(Element e) {
        Node child = e.getFirstChild();
        if (child instanceof CharacterData) {
           CharacterData cd = (CharacterData) child;
           return cd.getData();
        }
        return "?";
    }


}
