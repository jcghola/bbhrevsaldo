/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GolesRevNocturnaSaldo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.SocketTimeoutException;

/**
 *
 * @author Jsus
 */
public class SocketClienteTIGO
{	  

    private String serverName;
    private String path;
    private int port;
    private String XML;
	

    public SocketClienteTIGO(String serverName,String path, int port, String XML)
    {
        this.serverName = serverName;
        this.path = path;
        this.port = port;
        this.XML=XML;  
    }

  
  public String goSocket()throws Exception
  {
	
        String POST="POST "+path+" HTTP/1.0\r\n"+
				"Host: "+serverName+"\r\n"+
				"Content-Length: "+XML.length()+"\r\n"+
				"Content-Type: text/xml\r\n"+
				"Connection: close\r\n"+
				"\r\n"+
				XML;
   
    try{
        Socket socket = abreSocket(serverName, port);      
        if(socket != null){
            String respuesta = escribeLeeSocket(socket, POST);
            socket.close();
            return respuesta;
        }else{
            return "No se pudo abrir el socket, timed out";

        }
    }catch (Exception e){
      e.printStackTrace();      
      StringWriter errors = new StringWriter();
      e.printStackTrace(new PrintWriter(errors));        
      return errors.toString().replaceAll("\"", "").replaceAll("'", "");
    }
   
   // return getRespTIGOPrueba();
  }
  
  private String escribeLeeSocket(Socket socket, String writeTo) throws Exception
  {
    try 
    {
      //Timeout para que reciba informacion
      //socket.setSoTimeout(10*60*1000);
      //Escribimos el texto al socket.
      BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
      bufferedWriter.write(writeTo);
      bufferedWriter.flush();    
      
      //Leemos el texto del socket.
      BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
      StringBuilder sb = new StringBuilder();
      String str;
      while ((str = bufferedReader.readLine()) != null)
      {
        sb.append(str + "\n");
      }      
      //Cerramos el lector y devolvemos el resultado en String.
      bufferedWriter.close();
      bufferedReader.close();
      return sb.toString();
    } 
    catch (IOException e) 
    {
      e.printStackTrace();      
      StringWriter errors = new StringWriter();
      e.printStackTrace(new PrintWriter(errors));        
      return errors.toString().replaceAll("\"", "").replaceAll("'", "");
    }
  }
  

  private Socket abreSocket(String server, int port) throws Exception
  {
    Socket socket;
    
    // Creamos el socket con su timeout.
    try
    {
      InetAddress inteAddress = InetAddress.getByName(server);
      SocketAddress socketAddress = new InetSocketAddress(inteAddress, port);  
      //Creamos socket.
      socket = new Socket();
      //Determinamos el timeout de conexion, esto en milisegundos.
      int timeoutInMs = 10*1000;
      // 0 infinito      
      socket.connect(socketAddress, timeoutInMs);
      socket.setReuseAddress(true);
      
      return socket;
    } 
    catch (SocketTimeoutException ste) 
    {
      //System.err.println("Excedido el tiempo de espera para el socket.");
      ste.printStackTrace();
      return null;
    }
  }
  //funcion para pruebas
  private String getRespTIGOPrueba() {
        String[] resp={
        "HTTP/1.1 200 OK Date: Fri, 15 Nov 2013 12:59:58 GMT Server: Apache/2.2.25 (Unix) mod_ssl/2.2.25 OpenSSL/0.9.8e-fips-rhel5 PHP/5.3.8 mod_jk/1.2.37 Content-Length: 224 Connection: close Content-Type: text/plain  Content-Type: text/xml Connection: closed  <?xml version=\"1.0\" encoding=\"UTF-8\"?><billingcheck><msisdn>32026639</msisdn><transid>5348236799</transid><result>4</result><comment>SIN SALDO SUFICIENTE</comment></billingcheck>",
        "HTTP/1.1 200 OK Date: Thu, 21 Nov 2013 13:01:31 GMT Server: Apache/2.2.25 (Unix) mod_ssl/2.2.25 OpenSSL/0.9.8e-fips-rhel5 PHP/5.3.8 mod_jk/1.2.37 Content-Length: 206 Connection: close Content-Type: text/plain  Content-Type: text/xml Connection: closed  <?xml version=\\\"1.0\\\" encoding=\\\"UTF-8\\\"?><billingcheck><msisdn>40856261</msisdn><transid>5570256604</transid><result>0</result><comment>OK</comment></billingcheck>",
        "HTTP/1.1 502 Bad Gateway Date: Sat, 23 Nov 2013 16:10:16 GMT Server: Apache/2.2.25 (Unix) mod_ssl/2.2.25 OpenSSL/0.9.8e-fips-rhel5 PHP/5.3.8 mod_jk/1.2.37 Content-Length: 391 Connection: close Content-Type: text/html; charset=iso-8859-1  <!DOCTYPE HTML PUBLIC -//IETF//DTD HTML 2.0//EN> <html><head> <title>502 Bad Gateway</title> </head><body> <h1>Bad Gateway</h1> <p>The proxy server received an invalid response from an upstream server.<br /> </p> <hr> <address>Apache/2.2.25 (Unix) mod_ssl/2.2.25 OpenSSL/0.9.8e-fips-rhel5 PHP/5.3.8 mod_jk/1.2.37 Server at providerservices.tigo.com.gt Port 8080</address> </body></html>",
        "HTTP/1.1 200 OK Date: Sat, 23 Nov 2013 16:02:31 GMT Server: Apache/2.2.25 (Unix) mod_ssl/2.2.25 OpenSSL/0.9.8e-fips-rhel5 PHP/5.3.8 mod_jk/1.2.37 Content-Length: 225 Connection: close Content-Type: text/plain  Content-Type: text/xml Connection: closed  <?xml version=\"1.0\" encoding=\"UTF-8\"?><billingcheck><msisdn>47922020</msisdn><transid>5648798191</transid><result>3</result><comment>SISTEMA NO DISPONIBLE</comment></billingcheck>",
        "HTTP/1.1 200 OK Date: Sat, 23 Nov 2013 16:00:25 GMT Server: Apache/2.2.25 (Unix) mod_ssl/2.2.25 OpenSSL/0.9.8e-fips-rhel5 PHP/5.3.8 mod_jk/1.2.37 Content-Length: 230 Connection: close Content-Type: text/plain  Content-Type: text/xml Connection: closed  <?xml version=\"1.0\" encoding=\"UTF-8\"?><billingcheck><msisdn>50040054</msisdn><transid>5648744443</transid><result>7</result><comment>USUARIO EN ESTADO INVALIDO</comment></billingcheck>",
        "HTTP/1.1 200 OK Date: Sat, 23 Nov 2013 17:46:23 GMT Server: Apache/2.2.25 (Unix) mod_ssl/2.2.25 OpenSSL/0.9.8e-fips-rhel5 PHP/5.3.8 mod_jk/1.2.37 Content-Length: 228 Connection: close Content-Type: text/plain  Content-Type: text/xml Connection: closed  <?xml version=\"1.0\" encoding=\"UTF-8\"?><billingcheck><msisdn>40922273</msisdn><transid>5651121444</transid><result>5</result><comment>USUARIO NO EXISTE,BORRAR</comment></billingcheck> "};
        int index = (int)(Math.random() * 6);
        return resp[index];
   }

}
