/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package GolesRevNocturnaSaldo;

import java.util.Properties;
import java.io.*;  
import javax.mail.Message;  
import javax.mail.MessagingException;  
import javax.mail.Session;  
import javax.mail.Transport;  
import javax.mail.internet.InternetAddress;  
import javax.mail.internet.MimeMessage; 
/**
 *
 * @author Jsus
 */
public class MailSMS {  
    private final Properties properties = new Properties();        
    private String password;    
    private Session session; 
    private String mensaje;
    private String asunto;
    private MimeMessage message;
	
	public MailSMS(String asunto,String mensaje){
		this.mensaje=mensaje;
		this.asunto=asunto;	
	
		properties.put("mail.smtp.host", "190.111.4.133");
                //properties.put("mail.smtp.host", "192.168.16.2");
		properties.setProperty("mail.smtp.ssl.trust", "190.111.4.133");
		properties.put("mail.smtp.starttls.enable", "true");  
		properties.put("mail.smtp.port",25);  
		properties.put("mail.smtp.mail.sender","Plataforma SMS<plataformaSMS@telepromos.tv>");  
		properties.put("mail.smtp.user", "plataformaSMS");  
		properties.put("mail.smtp.auth", "true");
		password = "sms1#mx";
		session = Session.getDefaultInstance(properties);	
		message = new MimeMessage(session);
	}
    
	public void enviaEmail(){  
        try{ 
            FileInputStream file = new FileInputStream ("email.html");
            byte[] b = new byte[file.available ()];
            file.read(b);
            file.close ();
            String contenido = new String (b);
            mensaje = contenido.replace("|contenido|",mensaje);
            message.setFrom(new InternetAddress((String)properties.get("mail.smtp.mail.sender")));  
            message.setSubject(asunto);  
            message.setContent(mensaje,"text/html; charset=ISO-8859-1");
            Transport t = session.getTransport("smtp");  
            t.connect((String)properties.get("mail.smtp.user"), password);  
            t.sendMessage(message, message.getAllRecipients());  
            t.close();  
        }catch(Exception e) 
		{e.printStackTrace();return;}  
          
    } 
	
	public void agregaDestinatario(String destinatario,String tipo){
	try{ 
		if(tipo.equals("TO")){message.addRecipient(Message.RecipientType.TO, new InternetAddress(destinatario));}
                else if(tipo.equals("CC")){message.addRecipient(Message.RecipientType.CC, new InternetAddress(destinatario));}
                else if(tipo.equals("BCC")){message.addRecipient(Message.RecipientType.BCC, new InternetAddress(destinatario));}
                else{System.out.println("\nEl tipo de destinatario para el correo no existe!!");}
	}catch(Exception e) 
	{e.printStackTrace();return;}  
	
	}
  
}  
