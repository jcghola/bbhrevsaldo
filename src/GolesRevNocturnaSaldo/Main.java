package GolesRevNocturnaSaldo;

import crud.BillingCheckCRUD;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.apache.http.HttpHost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.routing.HttpRoute;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.hibernate.HibernateException;
import pojos.BillingCheckNumConCostoV;
import pojos.BillingVelocidad;
import pojos.ClientesBillingConsultarSaldo;


/*
 * @author Jsus 
 */

public class Main {
   
   public static void main(String[] args ){      
       if(args.length != 4){salidaCmd("\nError los parametros!!! \n");System.exit(1);}     
       int hilos=0, intentos=0, smail=0, porcentajeRevision=0;	                    
       try {			
            hilos=Integer.parseInt(args[0]);intentos=Integer.parseInt(args[1]);smail=Integer.parseInt(args[2]);porcentajeRevision=Integer.parseInt(args[3]);
       }catch (NumberFormatException nfe) { salidaCmd("\nError en los parametros\n");System.exit(1); }
      
       DateFormat formatter = new SimpleDateFormat("yyyyMMdd"); 
       Date hoy = new Date();                
       String fechaHoy = formatter.format(hoy);
       formatter = new SimpleDateFormat("HH:mm:ss");
       String horaInicio=formatter.format(hoy);
       String bitacora="";
       Map<String,BillingCheckNumConCostoV> montos=null;
       
       Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
               .<ConnectionSocketFactory> create()
               .register("http", PlainConnectionSocketFactory.INSTANCE)
               .build();
       PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
       cm.setMaxTotal(500); cm.setDefaultMaxPerRoute(500);
       cm.setMaxPerRoute(new HttpRoute(new HttpHost("providerservices.tigo.com.gt", 500)), 500);     
       HiloRevisaConexionesCaidas monitoreo=null;
       
       int intento=0;
       
       try{
           BillingCheckCRUD MD=new BillingCheckCRUD();
           montos=MD.getNumConCosto(porcentajeRevision);

           bitacora = "Inicio de la revisión nocturna de saldo : "+horaInicio+"---------------------------------------------------------------------------------------------------\n";
           salidaCmd("Revision nocturna de saldo\n");
           
           monitoreo = new HiloRevisaConexionesCaidas(cm);            
           monitoreo.start();

           while(true){       
               List<ClientesBillingConsultarSaldo> ListaClientes = MD.getClientesMtGt(fechaHoy,horaInicio);
               ArrayList<Hilo> listaHilos = new ArrayList<>();
               
               int totalRecobrar = ListaClientes.size();
               salidaCmd("\nTOTAL A PROCESAR: "+totalRecobrar+"\n\n");
               //System.exit(0);

               if(intento==0){
                   mandaCorreo(smail,"Se inicio la revision nocturna de saldo al "+porcentajeRevision+"%, Hora:"+horaInicio+" !!","<br/><br/> Se ha iniciado la revisión nicturna de saldo a través de BillingCheck.<br/>Inicio: "+horaInicio+"<br/>Favor de esperar la confirmacion de que ha terminado correctamente...<br/><br/>TOTAL A PROCESAR: "+totalRecobrar);
                   bitacora +="\nTOTAL A PROCESAR: "+totalRecobrar+"\n\n";
               }

               if(intento<intentos && totalRecobrar>0){
                   int MensjesPorHilo = totalRecobrar/hilos;
                   if(MensjesPorHilo!=0){
                       int o=0, cnt=0, d;
                       for(d=0;d<=totalRecobrar;d=d+MensjesPorHilo){
                           if(o!=d){ listaHilos.add(new Hilo(new ArrayList<>(ListaClientes.subList(o, d)),String.valueOf(cnt),cm, montos,porcentajeRevision)); }
                           cnt++;o=d;
                       }
                       if(o<totalRecobrar){ listaHilos.add(new Hilo(new ArrayList<>(ListaClientes.subList(o, totalRecobrar)),String.valueOf(cnt),cm, montos,porcentajeRevision)); }
                   }else{ listaHilos.add(new Hilo(new ArrayList<>(ListaClientes),String.valueOf(0),cm, montos,porcentajeRevision)); }
                   ListaClientes.clear();
                   
                   for (Thread hilo : listaHilos){hilo.start();} for (Thread hilo : listaHilos){hilo.join();}
                   listaHilos.clear();

               }else if(intento==intentos){
                    String resultado = "Numero de intentos vencidos, intento "+intento+" de "+intentos+" programados";
                    MandaResultadoFinal(bitacora,MD,totalRecobrar, smail, horaInicio, fechaHoy, porcentajeRevision,resultado);
                    salidaCmd(resultado); 
                    break;
               }else if(totalRecobrar==0){
                    String resultado ="Se terminaron de procesar todos los registros, intento "+intento+" de "+intentos+" programados";
                    MandaResultadoFinal(bitacora,MD,totalRecobrar, smail, horaInicio, fechaHoy, porcentajeRevision,resultado);
                    salidaCmd(resultado); 
                    break;
               }else{
                    salidaCmd("\nSe acabo!!!, condicion no contemplada");
                    break;
               }//fin del if
               intento++;

           }//fin del while
           
 
       }catch(InterruptedException | HibernateException | IOException  e){
           erroresToMailToFile(e,fechaHoy,bitacora,smail);
           e.printStackTrace();           
       }finally{
           if(monitoreo!=null){try{monitoreo.shutdown();}catch(Exception e){e.printStackTrace();}}
           if(montos!=null){try{montos.clear();}catch(Exception e){e.printStackTrace();}}
           try{
               cm.closeExpiredConnections();
               cm.closeIdleConnections(3, TimeUnit.SECONDS);
               cm.close();
           }catch(Exception e){e.printStackTrace();}                  
       }
            
    }
   
    private static void erroresToMailToFile(Exception e,String fechaHoy,String bitacora,int smail) {
      try {   
          salidaCmd("\nEntramos al error de consulta\n");
          StringWriter errors = new StringWriter();
          e.printStackTrace(new PrintWriter(errors));
        
          bitacora+="\nDescripcion....\n\n"+errors.toString();
          salidaCmd(errors.toString());
        
          String correo = "<br><br>Ocurrio una falla general al ejecutar a revision nocturna de saldo<br><br>"+"Bitacora: <br><br>"+bitacora.replaceAll("(\r\n|\n)", "<br>");
          String asunto = "Alerta REVISION NOCTURNA DE SALDO ERROR!!";
          mandaCorreo(smail,asunto,correo);
                 
          BufferedWriter out = new BufferedWriter(new FileWriter("bitacora/"+fechaHoy+".txt", true));
          out.write(bitacora+"\n\n");
          out.close();                    
        
      }catch (IOException ie) {         
         throw new RuntimeException("No se puede escribir en la bitacora", ie);         
      }
   }

   private static void salidaCmd ( String cadena ) {        
        System.out.println(cadena);
   }
   
   private static void mandaCorreo(int smail,String asunto,String correo){
        MailSMS mail = new MailSMS(asunto,correo);  
        mail.agregaDestinatario("jcgarcia@telepromos.tv","TO"); 
        //mail.agregaDestinatario("sistemas-mexico@telepromos.tv","CC");       
        if(smail==1){mail.enviaEmail();}   
   }
   
   private static void MandaResultadoFinal(String bitacora,BillingCheckCRUD MD,int totalRecobrar, int smail, String horaInicio, String fechaHoy, int porcentaje, String resultado)throws IOException{
        int ttl1=0; DateFormat formatter;
        Date fin = new Date();       

        formatter = new SimpleDateFormat("HH:mm:ss");
        String horaFin=formatter.format(fin);       
        
        bitacora +="Hora fin: "+horaFin;
        List l=MD.getBillingcheckResultado(fechaHoy, horaInicio);
        for (Object[] row: (List<Object[]>) l ) { bitacora +="\n"+(String)row[0]+": "+(Long)row[1]; ttl1+=safeLongToInt((Long)row[1]); }
        l.clear();
                
        String velHTML = "<br/><br/><bold>Detalle de velocidad</bold><br/><TABLE BORDER=1><TR><TD>H</TD><TD>M</TD><TD>TPS</TD></TR>",vel="\n\nDetalle de velocidad\nHH MM TTT";
        try{
            List lv=MD.getTPS(fechaHoy,horaInicio,fin);        
            for (BillingVelocidad v: (List<BillingVelocidad>) lv ) {velHTML+="<TR><TD>"+v.getH()+"</TD><TD>"+v.getM()+"</TD><TD>"+v.getT()+"</TD></TR>";vel+="\n"+v.getH()+":"+v.getM()+" "+v.getT();}            
        }catch(SQLException e){
            e.printStackTrace();
        }
        velHTML+="</TABLE>";vel+="\n";
         
        bitacora +="\n\nTOTAL OBTENIDOS: "+ttl1+"\nFallidos: "+totalRecobrar;
        String correo = "<br/>"+resultado+"<br/><br/>Se termino de ejecutar la revision nocturna de saldo<br><br>";
        String asunto = "Alerta Revisión nocturna de saldo al "+porcentaje+"% !!";
        
        correo+="Bitacora: <br/><br/>"+bitacora.replaceAll("(\r\n|\n)", "<br/>")+velHTML;
        
        mandaCorreo(smail,asunto,correo);

        BufferedWriter out = new BufferedWriter(new FileWriter("bitacora/"+fechaHoy+".txt", true));
        out.write(bitacora+vel+"\n\n");
        out.close(); 
        salidaCmd(bitacora+vel+"\n\n");
    }
   
    private static int safeLongToInt(long l) {
        if (l < Integer.MIN_VALUE || l > Integer.MAX_VALUE) {
            throw new IllegalArgumentException (l + " No se puede convertir a entero, no está dentro del rango.");
        }
        return (int) l;
    }
   
}
