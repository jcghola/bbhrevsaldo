package GolesRevNocturnaSaldo;

import java.net.*;
import java.io.*;
import java.text.Normalizer;

/**
 *
 * @author Jsus
 */
 
public class GetIndexMM {

        private String Username, Password, to, From, Message, Service, IdTransaction, Stringref, Id_Envio, Id_Clave;
        private int Ejecucion;
    
        public GetIndexMM(String Username,String Password, String to, String From, String Message, String Service,String Stringref,String IdTransaction,String Id_Envio, String Id_Clave,int Ejecucion){
        this.Username=Username;
        this.Password=Password;
        this.to=to;
        this.From=From;
        this.Message=Message;
        this.Service=Service;
        this.IdTransaction=IdTransaction;
        this.Stringref = Stringref;
        this.Id_Envio = Id_Envio;
        this.Id_Clave=Id_Clave;
        this.Ejecucion=Ejecucion;
        }        
        
	private String getText(String url) throws Exception {
        URL website = new URL(url);
        URLConnection connection = website.openConnection();
        BufferedReader in = new BufferedReader(
                                new InputStreamReader(
                                    connection.getInputStream()));

        StringBuilder response = new StringBuilder();
        String inputLine;

        while ((inputLine = in.readLine()) != null) 
            response.append(inputLine);
        in.close();

        return response.toString();
    }

    public Integer MandaMensaje() throws Exception {
        Integer msgindex=null;
        try{
        Message = Normalizer.normalize(Message, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
        String cadena =   "http://192.168.131.10/MMWebService/MessageMaster.aspx?"
                        + "Handler=SendMessage&"
                        + "Username="+Username+"&"
                        + "Password="+Password+"&"
                        + "to="+to+"&"
                        + "From="+From+"&"
                        + "Message="+URLEncoder.encode(Message)+"&"
                        + "Subject=&"
                        + "Service="+Service+"&"
                        + "Options=0&"
                        + "TimeToSend=&";
                        cadena += "Reference="+Id_Envio+"."+Id_Clave+"|"+Ejecucion+"-"+Stringref+"&";
                        //else{cadena += "Reference="+Stringref+"&";}                        
                        cadena += "BillText="+IdTransaction;
        
        String content = getText(cadena);
        //String content="index:9999";
        //System.out.println(content);
        String[] contentArr = content.split(":");
        msgindex = Integer.valueOf(contentArr[1].trim());        
        //System.out.println("Indexxxxxx:"+msgindex.trim());        
        
        }catch(Exception e){
        e.printStackTrace();
        }
        return msgindex;
    }
}
