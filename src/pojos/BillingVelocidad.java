/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pojos;

/**
 *
 * @author Jsus
 */
public class BillingVelocidad {
    private int h, m;
    private String t;

    public BillingVelocidad(int h, int m, String t) {
        this.h = h;
        this.m = m;
        this.t = t;
    }

    public int getH() {
        return h;
    }

    public int getM() {
        return m;
    }

    public String getT() {
        return t;
    }
    
}
