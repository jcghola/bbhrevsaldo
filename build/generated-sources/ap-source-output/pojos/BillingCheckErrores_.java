package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckErrores.class)
public abstract class BillingCheckErrores_ {

	public static volatile SingularAttribute<BillingCheckErrores, Integer> msgIndexOrigen;
	public static volatile SingularAttribute<BillingCheckErrores, Integer> result;
	public static volatile SingularAttribute<BillingCheckErrores, String> envio;
	public static volatile SingularAttribute<BillingCheckErrores, Integer> idOutbound;
	public static volatile SingularAttribute<BillingCheckErrores, Date> fecha;
	public static volatile SingularAttribute<BillingCheckErrores, Integer> msgIndexDestino;
	public static volatile SingularAttribute<BillingCheckErrores, Integer> idEnvio;
	public static volatile SingularAttribute<BillingCheckErrores, Long> idTransaccion;
	public static volatile SingularAttribute<BillingCheckErrores, String> dataxml;
	public static volatile SingularAttribute<BillingCheckErrores, String> comment;
	public static volatile SingularAttribute<BillingCheckErrores, Integer> idBillingCheck;

}

