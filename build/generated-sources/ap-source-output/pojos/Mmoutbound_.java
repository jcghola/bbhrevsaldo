package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Mmoutbound.class)
public abstract class Mmoutbound_ {

	public static volatile SingularAttribute<Mmoutbound, Integer> msgindex;
	public static volatile SingularAttribute<Mmoutbound, Date> msgtimestamp;
	public static volatile SingularAttribute<Mmoutbound, String> stringref;
	public static volatile SingularAttribute<Mmoutbound, String> billcode;
	public static volatile SingularAttribute<Mmoutbound, Date> lasttimestamp;
	public static volatile SingularAttribute<Mmoutbound, String> laststatus;
	public static volatile SingularAttribute<Mmoutbound, String> destination;
	public static volatile SingularAttribute<Mmoutbound, Integer> id;
	public static volatile SingularAttribute<Mmoutbound, String> servicefrom;
	public static volatile SingularAttribute<Mmoutbound, String> serviceto;
	public static volatile SingularAttribute<Mmoutbound, String> msgtext;
	public static volatile SingularAttribute<Mmoutbound, String> originator;
	public static volatile SingularAttribute<Mmoutbound, String> msgsubject;

}

