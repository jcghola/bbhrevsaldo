package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckClientesConSaldoDia.class)
public abstract class BillingCheckClientesConSaldoDia_ {

	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, Integer> id;
	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, Integer> idClave;
	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, Date> fecha;
	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, String> numCorto;
	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, Integer> msgindex;
	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, Integer> idEnvio;
	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, Integer> ejecucion;
	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, Integer> idCliente;
	public static volatile SingularAttribute<BillingCheckClientesConSaldoDia, Long> idTransaction;

}

