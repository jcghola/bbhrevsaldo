package pojos;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckNumConCostoV.class)
public abstract class BillingCheckNumConCostoV_ {

	public static volatile SingularAttribute<BillingCheckNumConCostoV, Integer> porcentaje;
	public static volatile SingularAttribute<BillingCheckNumConCostoV, Integer> id;
	public static volatile SingularAttribute<BillingCheckNumConCostoV, String> numCortoO;
	public static volatile SingularAttribute<BillingCheckNumConCostoV, String> carrier;
	public static volatile SingularAttribute<BillingCheckNumConCostoV, String> costo;
	public static volatile SingularAttribute<BillingCheckNumConCostoV, String> numCortoD;

}

