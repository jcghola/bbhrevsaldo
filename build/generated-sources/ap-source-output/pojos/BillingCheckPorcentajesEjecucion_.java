package pojos;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckPorcentajesEjecucion.class)
public abstract class BillingCheckPorcentajesEjecucion_ {

	public static volatile SingularAttribute<BillingCheckPorcentajesEjecucion, Integer> porcentaje;
	public static volatile SingularAttribute<BillingCheckPorcentajesEjecucion, Integer> id;
	public static volatile SingularAttribute<BillingCheckPorcentajesEjecucion, Integer> ejecucion;
	public static volatile SingularAttribute<BillingCheckPorcentajesEjecucion, String> tipo;

}

