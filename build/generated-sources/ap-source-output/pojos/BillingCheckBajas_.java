package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckBajas.class)
public abstract class BillingCheckBajas_ {

	public static volatile SingularAttribute<BillingCheckBajas, Integer> id;
	public static volatile SingularAttribute<BillingCheckBajas, Date> fecha;
	public static volatile SingularAttribute<BillingCheckBajas, Integer> idServicio;
	public static volatile SingularAttribute<BillingCheckBajas, String> numMovil;

}

