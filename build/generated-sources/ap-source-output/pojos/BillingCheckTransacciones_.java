package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckTransacciones.class)
public abstract class BillingCheckTransacciones_ {

	public static volatile SingularAttribute<BillingCheckTransacciones, Integer> msgIndexOrigen;
	public static volatile SingularAttribute<BillingCheckTransacciones, Integer> result;
	public static volatile SingularAttribute<BillingCheckTransacciones, String> envio;
	public static volatile SingularAttribute<BillingCheckTransacciones, Integer> idOutbound;
	public static volatile SingularAttribute<BillingCheckTransacciones, Date> fecha;
	public static volatile SingularAttribute<BillingCheckTransacciones, Integer> msgIndexDestino;
	public static volatile SingularAttribute<BillingCheckTransacciones, Integer> idEnvio;
	public static volatile SingularAttribute<BillingCheckTransacciones, Long> idTransaccion;
	public static volatile SingularAttribute<BillingCheckTransacciones, String> dataxml;
	public static volatile SingularAttribute<BillingCheckTransacciones, String> comment;
	public static volatile SingularAttribute<BillingCheckTransacciones, Integer> idBillingCheck;

}

