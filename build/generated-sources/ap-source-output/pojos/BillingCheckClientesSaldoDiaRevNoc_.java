package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckClientesSaldoDiaRevNoc.class)
public abstract class BillingCheckClientesSaldoDiaRevNoc_ {

	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNoc, Integer> id;
	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNoc, Date> fechaConSaldo;
	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNoc, String> numCorto;
	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNoc, Integer> idCliente;
	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNoc, String> numMovil;
	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNoc, Date> fechaRevision;
	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNoc, Long> idTransaction;

}

