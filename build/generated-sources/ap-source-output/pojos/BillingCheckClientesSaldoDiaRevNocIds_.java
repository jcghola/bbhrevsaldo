package pojos;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckClientesSaldoDiaRevNocIds.class)
public abstract class BillingCheckClientesSaldoDiaRevNocIds_ {

	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNocIds, Integer> id;
	public static volatile SingularAttribute<BillingCheckClientesSaldoDiaRevNocIds, Integer> idCliente;

}

