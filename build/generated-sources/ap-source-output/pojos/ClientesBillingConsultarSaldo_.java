package pojos;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ClientesBillingConsultarSaldo.class)
public abstract class ClientesBillingConsultarSaldo_ {

	public static volatile SingularAttribute<ClientesBillingConsultarSaldo, Integer> id;
	public static volatile SingularAttribute<ClientesBillingConsultarSaldo, String> numCorto;
	public static volatile SingularAttribute<ClientesBillingConsultarSaldo, Integer> idCliente;
	public static volatile SingularAttribute<ClientesBillingConsultarSaldo, String> carrier;
	public static volatile SingularAttribute<ClientesBillingConsultarSaldo, String> numMovil;

}

