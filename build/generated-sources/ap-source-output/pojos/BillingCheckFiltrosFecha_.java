package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckFiltrosFecha.class)
public abstract class BillingCheckFiltrosFecha_ {

	public static volatile SingularAttribute<BillingCheckFiltrosFecha, Integer> id;
	public static volatile SingularAttribute<BillingCheckFiltrosFecha, String> numCorto;
	public static volatile SingularAttribute<BillingCheckFiltrosFecha, Date> fechaFin;
	public static volatile SingularAttribute<BillingCheckFiltrosFecha, Date> fechaIni;

}

