package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingcheckControl.class)
public abstract class BillingcheckControl_ {

	public static volatile SingularAttribute<BillingcheckControl, String> userpwd;
	public static volatile SingularAttribute<BillingcheckControl, Integer> idClave;
	public static volatile SingularAttribute<BillingcheckControl, String> stringref;
	public static volatile SingularAttribute<BillingcheckControl, Integer> idCliente;
	public static volatile SingularAttribute<BillingcheckControl, String> destination;
	public static volatile SingularAttribute<BillingcheckControl, Integer> id;
	public static volatile SingularAttribute<BillingcheckControl, Date> lastupdate;
	public static volatile SingularAttribute<BillingcheckControl, Date> timetosend;
	public static volatile SingularAttribute<BillingcheckControl, String> username;
	public static volatile SingularAttribute<BillingcheckControl, Integer> idEnvio;
	public static volatile SingularAttribute<BillingcheckControl, String> msgtext;
	public static volatile SingularAttribute<BillingcheckControl, String> serviceto;
	public static volatile SingularAttribute<BillingcheckControl, String> originator;

}

