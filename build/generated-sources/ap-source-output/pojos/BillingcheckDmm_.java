package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingcheckDmm.class)
public abstract class BillingcheckDmm_ {

	public static volatile SingularAttribute<BillingcheckDmm, String> userpwd;
	public static volatile SingularAttribute<BillingcheckDmm, Integer> id;
	public static volatile SingularAttribute<BillingcheckDmm, String> username;
	public static volatile SingularAttribute<BillingcheckDmm, Date> timetosend;
	public static volatile SingularAttribute<BillingcheckDmm, Integer> idClave;
	public static volatile SingularAttribute<BillingcheckDmm, String> stringref;
	public static volatile SingularAttribute<BillingcheckDmm, Integer> idEnvio;
	public static volatile SingularAttribute<BillingcheckDmm, Integer> idCliente;
	public static volatile SingularAttribute<BillingcheckDmm, String> serviceto;
	public static volatile SingularAttribute<BillingcheckDmm, String> msgtext;
	public static volatile SingularAttribute<BillingcheckDmm, String> originator;
	public static volatile SingularAttribute<BillingcheckDmm, String> destination;

}

