package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ClientesMtGt.class)
public abstract class ClientesMtGt_ {

	public static volatile SingularAttribute<ClientesMtGt, Integer> idCategoria;
	public static volatile SingularAttribute<ClientesMtGt, Integer> clienteOriginal;
	public static volatile SingularAttribute<ClientesMtGt, Integer> numSinSaldo;
	public static volatile SingularAttribute<ClientesMtGt, Short> idStatus;
	public static volatile SingularAttribute<ClientesMtGt, Date> fechaEnvio;
	public static volatile SingularAttribute<ClientesMtGt, Integer> idClave;
	public static volatile SingularAttribute<ClientesMtGt, Date> fechaPosAlta;
	public static volatile SingularAttribute<ClientesMtGt, Integer> contenido;
	public static volatile SingularAttribute<ClientesMtGt, String> numCorto;
	public static volatile SingularAttribute<ClientesMtGt, Integer> idCliente;
	public static volatile SingularAttribute<ClientesMtGt, Integer> contadorEnvios;
	public static volatile SingularAttribute<ClientesMtGt, String> carrier;
	public static volatile SingularAttribute<ClientesMtGt, Date> fechaAlta;
	public static volatile SingularAttribute<ClientesMtGt, String> numMovil;
	public static volatile SingularAttribute<ClientesMtGt, Integer> numConSaldo;

}

