package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckHistoricoEjecuciones.class)
public abstract class BillingCheckHistoricoEjecuciones_ {

	public static volatile SingularAttribute<BillingCheckHistoricoEjecuciones, Integer> porcentaje;
	public static volatile SingularAttribute<BillingCheckHistoricoEjecuciones, Integer> id;
	public static volatile SingularAttribute<BillingCheckHistoricoEjecuciones, Date> fecha;
	public static volatile SingularAttribute<BillingCheckHistoricoEjecuciones, Integer> cobrados;
	public static volatile SingularAttribute<BillingCheckHistoricoEjecuciones, Integer> ejecucion;
	public static volatile SingularAttribute<BillingCheckHistoricoEjecuciones, String> tipo;
	public static volatile SingularAttribute<BillingCheckHistoricoEjecuciones, Date> fechaFin;

}

