package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckVariablesGlobales.class)
public abstract class BillingCheckVariablesGlobales_ {

	public static volatile SingularAttribute<BillingCheckVariablesGlobales, Integer> id;
	public static volatile SingularAttribute<BillingCheckVariablesGlobales, Date> fecha;
	public static volatile SingularAttribute<BillingCheckVariablesGlobales, String> valor;
	public static volatile SingularAttribute<BillingCheckVariablesGlobales, String> descripcion;
	public static volatile SingularAttribute<BillingCheckVariablesGlobales, String> variable;

}

