package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(EnviosMtGt.class)
public abstract class EnviosMtGt_ {

	public static volatile SingularAttribute<EnviosMtGt, Integer> idStatus;
	public static volatile SingularAttribute<EnviosMtGt, Date> fechaEnvio;
	public static volatile SingularAttribute<EnviosMtGt, String> idTransaccionBilling;
	public static volatile SingularAttribute<EnviosMtGt, String> idDeliver;
	public static volatile SingularAttribute<EnviosMtGt, Integer> numEjecucion;
	public static volatile SingularAttribute<EnviosMtGt, Integer> idEnvio;
	public static volatile SingularAttribute<EnviosMtGt, Integer> idCliente;
	public static volatile SingularAttribute<EnviosMtGt, String> mensaje;
	public static volatile SingularAttribute<EnviosMtGt, Integer> cmdStatus;
	public static volatile SingularAttribute<EnviosMtGt, String> lastStatus;

}

