package pojos;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(BillingCheckTransaccionesConsultaSaldo.class)
public abstract class BillingCheckTransaccionesConsultaSaldo_ {

	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, Integer> msgIndexOrigen;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, Integer> result;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, String> envio;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, Integer> idOutbound;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, Date> fecha;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, Integer> msgIndexDestino;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, Integer> idEnvio;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, Long> idTransaccion;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, String> dataxml;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, String> comment;
	public static volatile SingularAttribute<BillingCheckTransaccionesConsultaSaldo, Integer> idBillingCheck;

}

